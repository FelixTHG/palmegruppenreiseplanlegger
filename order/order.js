
// Global database:

var duration = [1, 2, 3, 4]; //vacation duration ranging from 1 to 4 weeks

var maxVisitorsPerRoom = 4; // simplifying  - max persons per room is equal for all hotels and apartments

// -------------Hotel and City classes----------------------

class Hotel {
  constructor(name, standard, specialService, specialServicePricePerWeek, maxVisitorsPerRoom, pricePerPersonPerNight) {
    this.name = name; //string
    this.standard = standard; // string standard rate 1-5 stars
    this.maxVisitorsPerRoom = maxVisitorsPerRoom; // int , max max visitors per room
    this.pricePerPersonPerNight = pricePerPersonPerNight; //pricer per 1-person room per night
    //this.roomPricesPerNight = this.roomPrices; //array of arrays containing roomSizeIndex and price i.e. [[1,1000],[2,2000]]
    this.specialService = specialService; //boolean, true of false
    this.specialServicePricePerWeek = specialServicePricePerWeek; //weekly price for special service provided by hotel
  }

  roomPricePerNight(VisitorsPerRoom){
    if (VisitorsPerRoom > maxVisitorsPerRoom) {
      alert ('Invalid number of visitors for hotel '+this.name); //for debugging
      return null;
    }
    else {
      return VisitorsPerRoom*this.pricePerPersonPerNight;
    }
  }

  get cleaningPricePerWeek(){
    // if standard > 2, then cleaning is included in price
    if (this.standard[0] > 2) {
      return 0;
    }
    else {
      return 50; // all apartments have same cleaning price
    }
  }

}

class City{
    constructor(name, distance, hotels){
  this.name = name; //string
  this.distance = distance; //int distance from
  this.hotels = hotels; // array containing Hotel instances
  }
}



// -------------creating city and hotel instances----------------------

var Chania = new Hotel("Chania Hotel","3*",true, 300, maxVisitorsPerRoom, 300);
var GreatAlexander = new Hotel("Great Alexander Hotel","4*",true,400,maxVisitorsPerRoom, 520 );
var Irakleon = new Hotel("Irakleon Apartments","2*",true, 240, maxVisitorsPerRoom, 240 );
var SouthernRelaxation = new Hotel("Southern Relaxation Hotel","5*",true,580,maxVisitorsPerRoom, 580 );
var SouthernChill = new Hotel("Southern Chill Apartments","2*",true,280,maxVisitorsPerRoom, 280 );
var RegataTavern = new Hotel("Regata Tavern Hotel","3*",false, null,maxVisitorsPerRoom, 280 );
var TheFlyingBroom = new Hotel("The Flying Broom Hotel","4*",true, 450,maxVisitorsPerRoom, 450 );
var HighLifeSuite = new Hotel("High Life Suite Hotel","5*",true, 650,maxVisitorsPerRoom, 650 );
var BudgetcutLodgings = new Hotel("Budgetcut Lodgings Apartments","1*",true, 190,maxVisitorsPerRoom, 190 );

var Malaga = new City("Malaga",3500, [GreatAlexander, RegataTavern, BudgetcutLodgings] );
var Dubrovnik = new City("Dubrovnik",2600,[TheFlyingBroom, SouthernChill, SouthernRelaxation]);
var Lisboa = new City("Lisboa",2400, [HighLifeSuite, Chania, Irakleon]);

var cities = [Malaga,Dubrovnik,Lisboa];

// -------------script functions----------------------

function getCityInstance(cityName){
  for (var i = 0; i < cities.length; i++) {
    if (cities[i].name == cityName) {
      return cities[i];
    }
  }
  // alert('getCityInstance() error!'); //for debugging
  return null;
}

function getHotelInstance(cityName, hotelName){
  //this function has two input parameters (strings) because it's assumed that hotels with same names can be in different cities
  //hotel "hotelname" exists in city "cityName"
  var city;
  var hotel;

  for (var i = 0; i < cities.length; i++) {
    city = cities[i];
    if (city.name == cityName) {
      for (var i = 0; i < city.hotels.length; i++) {
        hotel = city.hotels[i];
        if (hotel.name == hotelName) {
          return hotel; // return first hotel that matches the condition
        }
      }
    }
  }

  //alert('getHotelInstance() error!'); // for debugging
  return null;
}

function getSelectedOptionAsString(selectedID){
  var selector = document.getElementById(selectedID);
  if (selector.options.length > 0) {
    //if select box is NOT empty
      return selector.options[selector.selectedIndex].text;
  }
  else {
    //alert('getSelectedOptionAsString() error! Select box is empty'); //for debugging
    return null;
  }
}

function getSelectedOptionValue(selectedID){
  var selector = document.getElementById(selectedID);
  if (selector.options.length > 0) {
    //if select box is NOT empty
      return selector.options[selector.selectedIndex].value;
  }
  else {
    //alert('getSelectedOptionValue() error! Select box is empty'); //for debugging
    return null;
  }
}

//------------------- Form functions---------------------

function addCities(){
  var element = document.getElementById('city-selector');
  element.innerHTML ="";

  for (var i = 0; i < cities.length; i++) {
    element.innerHTML = element.innerHTML +
                        "<option value='" +cities[i].name+ "'>" +
                        cities[i].name + " (Bilettpris t/r: "+ cities[i].distance + "kr per person)" +
                        "</option>";
  }
}

function addStandard(){
  var element = document.getElementById('standard-selector');
  element.innerHTML ="";
  for (var i = 1; i < 6; i++) {
    element.innerHTML = element.innerHTML + "<option value='" +i.toString()+ "'>"+i+"*"+"</option>";
  }
}

function addHotels(){
  //hotels are displayed based on the requested city and hotel statdard
  var hotelSelector = document.getElementById('hotel-selector');
  hotelSelector.innerHTML ="";

  //getting city instance based on the selected city
  var selectedCityAsString = getSelectedOptionValue('city-selector');
  var selectedCityAsCity = getCityInstance(selectedCityAsString);

  //getting selected standard as string
  var selectedStandard = getSelectedOptionValue('standard-selector');

  //iterating though cities' hotels to fill in the ones mathing the standard
  for (var i = 0; i < selectedCityAsCity.hotels.length; i++) {
    if (selectedCityAsCity.hotels[i].standard[0] == selectedStandard) {
      hotelSelector.innerHTML = hotelSelector.innerHTML +
                                "<option value='" +selectedCityAsCity.hotels[i].name + "'>" +
                                selectedCityAsCity.hotels[i].name+"</option>";
    }
  }

  //alerting the user if there is no hotel match
  var selectedHotelAsString = getSelectedOptionValue('hotel-selector');
  if (typeof selectedHotelAsString === "object") { //if getSelectedOptionValue() returned null i.e hotel-selector was epmty
      alert('Ingen hotell av standard ' +
            getSelectedOptionAsString('standard-selector') +
            'er tilgjengelige i '+
            getSelectedOptionAsString('city-selector') +
            ". Vennligst velg annnen standard/by" );
  }
}

function addSpecialService(){
  //Special service option is based on wheter a particular hotel can provide it or not
  var specialServiceSelector = document.getElementById('special-service-selector');
  specialServiceSelector.innerHTML ="";
  specialServiceSelector.disabled = false;
  document.getElementById('special-service-not-available').innerHTML = "";

  //getting city instance based on the selected city
  var selectedCityAsString = getSelectedOptionValue('city-selector');
  //getting hotel instance based on the selected hotel
  var selectedHotelAsString = getSelectedOptionValue('hotel-selector');

  if (typeof selectedHotelAsString === "object") { //if getSelectedOptionAsString() returned null i.e hotel-selector was epmty
    specialServiceSelector.disabled = true;
    document.getElementById('special-service-not-available').innerHTML = 'Ingen hotell valgt';
  }
  else {
    var selectedHotelAsInstance = getHotelInstance(selectedCityAsString,selectedHotelAsString);

    //If hotel is of 5* standard then all is inclusive
    if (selectedHotelAsInstance.standard[0] == 5) {
      specialServiceSelector.disabled = true;
      document.getElementById('special-service-not-available').innerHTML = 'Hotellet er all inclusive';
    }
    else {
      //Is special service provided ?
      if (selectedHotelAsInstance.specialService == true) {
          specialServiceSelector.innerHTML = specialServiceSelector.innerHTML +
                                             "<option value='Yes'>" +
                                             " Ja (Telefon + Fjernsyn + Internett " + selectedHotelAsInstance.specialServicePricePerWeek +" kr per uke)"+
                                             "</option>" +
                                             "<option value='No'>Nei</option>";
      }
      else {
        specialServiceSelector.disabled = true;
        document.getElementById('special-service-not-available').innerHTML = 'Hotellet tilbyr ikke tillegstjenester';
      }
    }
  }
}

function addCleaningService(){
  //cleaning service option can be chosen only for apartments i.e hotels of 1* or 2* standard
  var cleaningServiceSelector = document.getElementById('cleaning-service-selector');
  cleaningServiceSelector.innerHTML ="";
  cleaningServiceSelector.disabled = false;
  document.getElementById('cleaning-service-not-available').innerHTML = "";

  //getting city instance based on the selected city
  var selectedCityAsString = getSelectedOptionValue('city-selector');
  //getting hotel instance based on the selected hotel
  var selectedHotelAsString = getSelectedOptionValue('hotel-selector');

  if (typeof selectedHotelAsString === "object") { //if getSelectedOptionAsString() returned null i.e hotel-selector was epmty
    cleaningServiceSelector.disabled = true;
    document.getElementById('cleaning-service-not-available').innerHTML = 'Ingen hotell valgt';
  }
  else {
    var selectedHotelAsInstance = getHotelInstance(selectedCityAsString,selectedHotelAsString);

    //Is special service provided ? I.e is apartment selected ?
    if (selectedHotelAsInstance.standard[0] < 3) {
      cleaningServiceSelector.innerHTML = cleaningServiceSelector.innerHTML +
                                          "<option value='Yes'>"
                                          +"Ja (" + selectedHotelAsInstance.cleaningPricePerWeek + " kr per uke)" +
                                          "</option>" +
                                          "<option value='No'>Nei</option>";
    }
    else {
      cleaningServiceSelector.disabled = true;
      document.getElementById('cleaning-service-not-available').innerHTML = 'Inkludert i prisen';
    }
  }

}

function addPersonsPerRoom(){
  var element = document.getElementById('persons-per-room-selector');
  element.innerHTML ="";
  element.disabled = false;

  //getting city instance based on the selected city
  var selectedCityAsString = getSelectedOptionValue('city-selector');
  //getting hotel instance based on the selected hotel
  var selectedHotelAsString = getSelectedOptionValue('hotel-selector');

  if (typeof selectedHotelAsString === "object") { //if getSelectedOptionAsString() returned null i.e hotel-selector was empty
    element.disabled = true;
    document.getElementById('persons-per-room-not-available').innerHTML = 'Ingen hotell valgt';
  }
  else {
    var selectedHotelAsInstance = getHotelInstance(selectedCityAsString,selectedHotelAsString);
    document.getElementById('persons-per-room-not-available').innerHTML = '';
    for (var i = 1; i <= selectedHotelAsInstance.maxVisitorsPerRoom ; i++) {
      element.innerHTML = element.innerHTML +
                          "<option value='" + i.toString()+ "'>"+
                          i + " (kr "+ selectedHotelAsInstance.roomPricePerNight(i) +" per natt)"+
                          "</option>";
    }
  }
}

function addDuration(){
  //antar at ALLE hotellene kan tilby turvarighet fra 1 til 4 uker.
  var element = document.getElementById('duration-selector');
  element.innerHTML ="";
  for (var i = 1; i <= duration.length; i++) {
    if (i == 1) {
      element.innerHTML = element.innerHTML + "<option value='" +i.toString()+ "'>"+i+ " uke" + "</option>";
    }
    else {
      element.innerHTML = element.innerHTML + "<option value='" +i.toString()+ "'>"+i+ " uker" + "</option>";
    }
  }
}

//-------------------Price variables and functions--------------

var discountRate = 0.2;
var discount = false;

function giveDiscount(){
  var element = document.getElementById('Yes');
  if (element.checked) {
    discount = true;
  }
  else {
    discount = false;
  }
}

function calculatePrice(){
  var totalPrice = null;

  //getting city instance based on the selected city
  var selectedCityAsString = getSelectedOptionValue('city-selector');
  //getting hotel instance based on the selected hotel
  var selectedHotelAsString = getSelectedOptionValue('hotel-selector');

  if (typeof selectedHotelAsString === "object") { //if getSelectedOptionAsString() returned null i.e hotel-selector was empty
    return null;
  }
  else {
    var selectedHotelAsInstance = getHotelInstance(selectedCityAsString,selectedHotelAsString);

    //price variables;
    var roomPricePerWeek = null;
    var specialServicePricePerWeek = null;
    var cleaningPricePerWeek = null;
    var farePrice = null;

    //total price = (pricePerRoom*7 +additional service + cleaning service)* duration
    var personsPerRoom = getSelectedOptionValue('persons-per-room-selector');
    roomPricePerWeek = selectedHotelAsInstance.roomPricePerNight(personsPerRoom)*7;

    // sier bilettprisen per person = avstanden til byen
    farePrice = getCityInstance(selectedCityAsString).distance * personsPerRoom;

    //if "Yes" was chosen in the select box, then update addServiePricePerWeek, otherwise do nothing
    if (getSelectedOptionValue('special-service-selector') == 'Yes' ) {
      specialServicePricePerWeek = selectedHotelAsInstance.specialServicePricePerWeek;
    }

    if (getSelectedOptionValue('cleaning-service-selector') == 'Yes' ) {
      cleaningPricePerWeek = selectedHotelAsInstance.cleaningPricePerWeek;
    }

    //array containing all the prices
    var priceArray = [roomPricePerWeek,specialServicePricePerWeek,cleaningPricePerWeek];
    for (var i = 0; i < priceArray.length; i++) {
      //if priceArray element is not null
      if (typeof priceArray[i] !== "object") {
        totalPrice += priceArray[i];
      }
    }

    //multiplying price/week * numberOfWeeks
    totalPrice = totalPrice * getSelectedOptionValue('duration-selector') + farePrice;
    giveDiscount();

    return discount == true ? totalPrice*(1-discountRate) : totalPrice ;
  }
}

function addPrice(){
  var element = document.getElementById('price');
  // var selectedCityAsString = getSelectedOptionAsString('city-selector');
  //getting hotel instance based on the selected hotel
  var selectedHotelAsString = getSelectedOptionValue('hotel-selector');

  if (typeof selectedHotelAsString === "object") { //if getSelectedOptionAsString() returned null i.e hotel-selector was epmty
    element.innerHTML = 'Pris : N/A';
  }
  else {
    element.innerHTML = "Prisen er: " + calculatePrice() +" NOK";
  }
}

//-------------------Refresh functions--------------

function refreshSubmitButton(){
  var selectedHotelAsString = getSelectedOptionAsString('hotel-selector');
  if (typeof selectedHotelAsString === "object") { //if getSelectedOptionAsString() returned null i.e hotel-selector was epmty
    //then impossible to make an order
    document.getElementById('submit-button').disabled = true;
  }
  else {
    document.getElementById('submit-button').disabled = false;
  }
}

function refreshForm(){
  addHotels();
  addSpecialService();
  addCleaningService();
  addPersonsPerRoom();
  addPrice();
  refreshSubmitButton();
}



