var isUserRegistered = false;

function isUserRegistered(){
  return isUserRegistered;
}

function register(){
  isUserRegistered = true;
}

function unRegister (){
  isUserRegistered = false;
}


/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

//function to resize the header dimentions
function resizeHeader(){
    var buttonContainerHeight = document.getElementsByClassName('header-buttons')[0].offsetHeight; // returns number x
    var newHeaderHeigth;
    var test = window.getComputedStyle(document.getElementsByClassName('header-buttons')[0], null).getPropertyValue("position");
    //if button container position is inherited, then it wil be displayed in line with the logo
    if (window.getComputedStyle(document.getElementsByClassName('header-buttons')[0], null).getPropertyValue("position") == "fixed") {
      //need to use getComputedStyle() since .css stylesheet is retrieved. getElementById().style.heigth refers only to html element style attribute
      var buttonContainterTopOffset = window.getComputedStyle(document.getElementsByClassName('header-buttons')[0], null).getPropertyValue("top"); // returns string "XXXXpx"
      var length = buttonContainterTopOffset.length;
      //making string containing only offset value without " px"
      buttonContainterTopOffset = buttonContainterTopOffset.substring(0,length-2);
      //new header heigth will be:
      newHeaderHeigth = (buttonContainerHeight + 2*Number(buttonContainterTopOffset)) + "px";
    }
    //if button container position is unset, then it wil be displayed below the logo
    else if (window.getComputedStyle(document.getElementsByClassName('header-buttons')[0], null).getPropertyValue("position") == "static") {
      newHeaderHeigth = (document.getElementById('logo-image').offsetHeight + buttonContainerHeight + 15 ) + "px"; // 15 is custom margin heigth
    }
    else {
        alert('error in resizeHeader() function');
    }
    document.getElementById('header').style.height  = newHeaderHeigth;
    document.getElementsByClassName('FrontImageAndtext')[0].style.marginTop  = newHeaderHeigth;
  }

  //resize the header heigth with respect to the buttons
  window.onresize = function resizeHeader(){
      var buttonContainerHeight = document.getElementsByClassName('header-buttons')[0].offsetHeight; // returns number x
      var newHeaderHeigth;
      var test = window.getComputedStyle(document.getElementsByClassName('header-buttons')[0], null).getPropertyValue("position");
      //if button container position is inherited, then it wil be displayed in line with the logo
      if (window.getComputedStyle(document.getElementsByClassName('header-buttons')[0], null).getPropertyValue("position") == "fixed") {
        //need to use getComputedStyle() since .css stylesheet is retrieved. getElementById().style.heigth refers only to html element style attribute
        var buttonContainterTopOffset = window.getComputedStyle(document.getElementsByClassName('header-buttons')[0], null).getPropertyValue("top"); // returns string "XXXXpx"
        var length = buttonContainterTopOffset.length;
        //making string containing only offset value without " px"
        buttonContainterTopOffset = buttonContainterTopOffset.substring(0,length-2);
        //new header heigth will be:
        newHeaderHeigth = (buttonContainerHeight + 2*Number(buttonContainterTopOffset)) + "px";
      }
      //if button container position is unset, then it wil be displayed below the logo
      else if (window.getComputedStyle(document.getElementsByClassName('header-buttons')[0], null).getPropertyValue("position") == "static") {
        newHeaderHeigth = (document.getElementById('logo-image').offsetHeight + buttonContainerHeight + 15 ) + "px"; // 15 is custom margin heigth
      }
      else {
          alert('error in resize() function');
      }
      document.getElementById('header').style.height  = newHeaderHeigth;
      document.getElementsByClassName('FrontImageAndtext')[0].style.marginTop  = newHeaderHeigth;
    }



