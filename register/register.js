

function addDays(){
  var element = document.getElementById('DaySelector');
  element.innerHTML ="";
  for (var i = 1; i < 32; i++) {
    element.innerHTML = element.innerHTML + "<option value='" + i + "'>"+i+"</option>";
  }
}

function addMonths(){
  var element = document.getElementById('MonthSelector');
  element.innerHTML ="";
  for (var i = 1; i < 13; i++) {
    element.innerHTML = element.innerHTML + "<option value='" + i + "'>"+i+"</option>";
  }
}

function addYears() {
var element = document.getElementById('YearSelector');
element.innerHTML ="";
  for (var i = 2017; i > 1949; i--) {
    element.innerHTML = element.innerHTML + "<option value='" + i + "'>"+i+"</option>";
  }
}

//----------------for age validation----------------
function getSelectedOptionAsString(selectedID){
  var selector = document.getElementById(selectedID);
  if (selector.options.length > 0) {
    //if select box is NOT empty
      return selector.options[selector.selectedIndex].text;
  }
  else {
    //alert('getSelectedOptionAsString() error! Select box is empty'); //for debugging
    return null;
  }
}

function getCurrentDate(){
  var today = new Date();
  var day = today.getDate();
  var month = today.getMonth()+1; //January is 0!
  var year = today.getFullYear();

  return [day,month,year];
}

function isAgeValid(){
debugger;

  var chosenDay = Number(getSelectedOptionAsString('DaySelector'));
  var chosenMonth = Number(getSelectedOptionAsString('MonthSelector'));
  var chosenYear = Number(getSelectedOptionAsString('YearSelector'));

  var currentDay = Number(getCurrentDate()[0]);
  var currentMonth = Number(getCurrentDate()[1]);
  var currentYear = Number(getCurrentDate()[2]);

  var isAgeValid = true;

  //check if the person is under 18 years old
  if (chosenYear > currentYear-18) {
    isAgeValid = false;
  }
  else if (chosenYear == currentYear-18) {
      if (chosenMonth > currentMonth) {
        isAgeValid = false;
      }
      else if (chosenMonth == currentMonth) {
        if (chosenDay > currentDay) {
          isAgeValid = false;
        }
      }
    }

  if (isAgeValid) {
    return true;
  } else {
    alert('Du må være over 18 år for å registrere deg');
    return false;
  }
}



